import Vue from "vue";
import VueRouter from "vue-router";
import Home from "../views/Home.vue";
import Product from "../views/Product.vue";
import Cart from "../views/Cart.vue";
import Checkout from "../views/Checkout.vue";
import AdminAuth from "../views/Auth.vue";
import AdminDashboard from "../views/AdminDashboard.vue";

import store from "../store";

import AdminApi from "../api/admin";

Vue.use(VueRouter);

function isAuthenticated() {
  return new Promise(async (resolve, reject) => {
    if (!store.getters.adminToken) {
      return resolve(false);
    }
    try {
      const authRequest = await AdminApi.post(
        "/validate-token",
        {},
        {
          headers: {
            Authorization: `Bearer ${store.getters.adminToken}`,
          },
        }
      );
      resolve(authRequest.data.valid);
    } catch (error) {
      resolve(false);
    }
  });
}

const routes = [
  {
    path: "/",
    name: "Home",
    component: Home,
  },
  {
    path: "/product/:slug",
    name: "Product",
    component: Product,
  },
  {
    path: "/cart",
    name: "Cart",
    component: Cart,
  },
  {
    path: "/checkout",
    name: "Checkout",
    component: Checkout,
  },
  {
    path: "/admin/login",
    name: "Admin Auth",
    component: AdminAuth,
    beforeEnter: (to, from, next) => {
      if (store.getters.isAdminLoggedIn) {
        next("/admin/dashboard");
      } else {
        next();
      }
    },
  },
  {
    path: "/admin/dashboard",
    name: "Admin Dashboard",
    component: AdminDashboard,
    beforeEnter: async (to, from, next) => {
      try {
        const validLogin = await isAuthenticated();
        console.log(validLogin);
        if (validLogin) {
          if (store.getters.layout !== "admin") {
            store.commit("SET_LAYOUT", "admin");
          }
          next();
        } else {
          store.commit("SET_LAYOUT", "default");
          store.commit("RESET_ADMIN");
          next("/admin/login");
        }
      } catch (error) {
        store.commit("SET_LAYOUT", "default");
        store.commit("RESET_ADMIN");
        next("/admin/login");
      }
    },
  },
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes,
});

router.beforeEach((to, from, next) => {
  if (!to.fullPath.includes("/admin")) {
    store.commit("SET_LAYOUT", "default");
  }
  next();
});

export default router;
