import Vue from "vue";
import Vuex from "vuex";

import createPersistedState from "vuex-persistedstate";
import * as Cookies from "js-cookie";

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    cart: [],
    email: "",
    name: "",
    admin: {
      isLoggedIn: false,
      token: "",
    },
    layout: "default",
  },
  mutations: {
    ADD_TO_CART(state, productID) {
      state.cart.push(productID);
    },
    REMOVE_CART_ITEM(state, productID) {
      const cartIndex = state.cart.findIndex((item, index) => {
        return item === productID;
      });
      state.cart.splice(cartIndex, 1);
    },
    RESET_CART(state) {
      state.cart = [];
    },
    SET_ADMIN_LOGIN_STATE(state, payload) {
      state.admin.isLoggedIn = payload;
    },
    SET_ADMIN_TOKEN(state, token) {
      state.admin.token = token;
    },
    SET_LAYOUT(state, layout) {
      state.layout = layout;
    },
    RESET_ADMIN(state) {
      state.admin.isLoggedIn = false;
      state.admin.token = "";
    },
  },
  actions: {},
  modules: {},
  getters: {
    cartCount(state) {
      return state.cart.length;
    },
    addedProducts(state) {
      return state.cart;
    },
    layout(state) {
      return state.layout;
    },
    isAdminLoggedIn(state) {
      return state.admin.isLoggedIn;
    },
    adminToken(state) {
      return state.admin.token;
    },
  },
  plugins: [
    createPersistedState({
      storage: {
        getItem: (key) => Cookies.get(key),
        setItem: (key, value) =>
          Cookies.set(key, value, { expires: 3, secure: false }),
        removeItem: (key) => Cookies.remove(key),
      },
    }),
  ],
});
